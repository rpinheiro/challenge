# -*- coding: utf-8 -*-

from django.contrib import admin
from challenge.billing.models import Call, CallDetail

admin.site.register(Call)
admin.site.register(CallDetail)
