# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from dateutil.relativedelta import *
import decimal
from django.shortcuts import render

from rest_framework import viewsets
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework import status

from challenge.billing.models import *
from challenge.billing.serializers import CallDetailSerializer, CallSerializer, BillingSerializer


def index(request):
    return render(request, "index.html")


class CallDetailViewSet(viewsets.ModelViewSet):
    queryset = CallDetail.objects.all()
    serializer_class = CallDetailSerializer


class CallViewSet(viewsets.ModelViewSet):
    queryset = Call.objects.all()
    serializer_class = CallSerializer


class CallView(APIView):
    parser_classes = (JSONParser,)

    def post(self, request, format=None):
        call_events = request.data['data']
        for ce in call_events:
            if len(ce) == 6:
                try:
                    source = ce['source']
                    destinatiom = ce['destination']
                    timestamp = ce['timestamp']
                    call_id = ce['call_id']

                    call = Call()
                    call.source = source
                    call.destination = destinatiom
                    call.call_id = call_id

                    call.save()

                    detail = CallDetail()
                    detail.call = call
                    detail.timestamp = timestamp
                    detail.type = RECORD_TYPE[0]

                    detail.save()

                    for d in CallDetail.objects.filter(call_id=call_id):
                        if d.call is None:
                            d.call = call
                            d.save()

                except:
                    return Response("ERROR WHILE PROCESSING REQUEST", status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            elif len(request.data) == 4:
                try:
                    timestamp = ce['timestamp']
                    call_id = ce['call_id']
                    try:
                        call = Call.objects.get(call_id=call_id)
                    except:
                        call = None

                    detail = CallDetail()
                    detail.call = call
                    detail.timestamp = timestamp

                    detail.type = RECORD_TYPE[1]
                    detail.save()
                except:
                    pass
            else:
                return Response("ERROR WHILE PROCESSING REQUEST", status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response("SUCESS", status=status.HTTP_200_OK)

def convert_timedelta(duration):
    days, seconds = duration.days, duration.seconds
    hours = days * 24 + seconds // 3600
    minutes = (seconds % 3600) // 60
    seconds = (seconds % 60)

    return '{}h{}m{}s'.format(hours, minutes, seconds)


def billing(serializer):
    standing_charge = decimal.Decimal('0.36')
    call_charge = decimal.Decimal('0.09')

    subscriber = serializer.validated_data['subscriber']
    period_month = serializer.validated_data['period_month']
    period_year = serializer.validated_data['period_year']

    start = '-'.join([str(period_year), str(period_month), '1']) + ' 00:00:00'
    start = datetime.strptime(start, '%Y-%m-%d %H:%M:%S')

    end = start + relativedelta(months=+1)
    end = end - timedelta(days=1)
    end = end.replace(hour=23, minute=59, second=59)

    end_events = CallDetail.objects.filter(type=1,
                                           timestamp__gte=start,
                                           timestamp__lte=end,
                                           call__source=subscriber)

    list_dict = []

    for end_event in end_events:
        try:
            start_event = CallDetail.objects.get(call__call_id=end_event.call.call_id, type=0)
        except:
            # not found the start call event. this call will be discarded from billing
            continue

        total_call_time = end_event.timestamp - start_event.timestamp
        duration = convert_timedelta(total_call_time)

        start_point = start_event.timestamp

        loop = True
        minutes = 0
        while loop:
            if start_point <= end_event.timestamp:
                if start_point.hour not in [22, 23, 0, 1, 2, 3, 4, 5, 6]:
                    minutes += 1
                start_point = start_point + timedelta(minutes=1)
            else:
                loop = False

        price = standing_charge + (call_charge * decimal.Decimal(minutes))

        dict = {"destination": start_event.call.destination,
                "call_start_date": start_event.timestamp.strftime("%d/%m/%Y"),
                "call_start_time": start_event.timestamp.strftime("%H:%M:%S"),
                "call_duration": duration,
                "call_price": 'R$ ' + str(price)}

        list_dict.append(dict)

    return list_dict


class BillingView(APIView):
    serializer_class = BillingSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context={'request': request})

        try:
            if serializer.is_valid(raise_exception=True):
                ret = billing(serializer)
                return Response(ret, status=status.HTTP_200_OK)

        except Exception as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    def get_serializer_class(self):
        assert self.serializer_class is not None, (
                "'%s' should either include a `serializer_class` attribute, "
                "or override the `get_serializer_class()` method."
                % self.__class__.__name__
        )

        return self.serializer_class

    def get_serializer_context(self):
        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self
        }