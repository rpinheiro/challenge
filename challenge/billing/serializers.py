# -*- coding: utf-8 -*-
from abc import ABC

from rest_framework import serializers

from rest_framework import serializers
from challenge.billing.models import Call, CallDetail


class CallDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = CallDetail
        fields = ['id',
                  'url',
                  'type',
                  'timestamp',
                  'call'
                  ]

        read_only_fields = ('call',)


class CallSerializer(serializers.ModelSerializer):
    details = CallDetailSerializer(many=True)

    class Meta:
        model = Call
        fields = ['id',
                  'url',
                  'source',
                  'destination',
                  'details'
                  ]

    def create(self, validated_data):
        details = validated_data.pop('details')
        call = Call.objects.create(**validated_data)

        for detail in details:
            CallDetail.objects.create(**detail, call=call)

        return call


class BillingSerializer(serializers.Serializer):
    subscriber = serializers.CharField(max_length=11, required=True)
    period_month = serializers.IntegerField(required=False)
    period_year = serializers.IntegerField(required=False)

    def validate_period_month(self, value):

        if value is not None:
            if value not in range(1, 13):
                raise serializers.ValidationError("Month value is not valid. Please, provide a valid value")

        return value

    def validate_period_year(self, value):
        if value is not None:
            if len(str(abs(value))) != 4:
                raise serializers.ValidationError("Year value is not valid. Please, provide a valid value")

        return value