# -*- coding: utf-8 -*-

from django.db import models

RECORD_TYPE = ((0, u'Start'),
               (1, u'End')
               )


class Call(models.Model):
    source = models.CharField(max_length=11, blank=False, null=False)
    destination = models.CharField(max_length=11, blank=False, null=False)
    call_id = models.IntegerField(blank=False, null=False)

    def __str__(self):
        return '-'.join([str(self.call_id), self.source])

    @property
    def details(self):
        return self.calldetail_set.all()


class CallDetail(models.Model):
    call = models.ForeignKey(Call, null=False, blank=False, on_delete=models.PROTECT)
    type = models.IntegerField(choices=RECORD_TYPE, default=0)
    timestamp = models.DateTimeField(blank=False, null=False)

    def __str__(self):
        return '/'.join([self.call.source, self.call.destination, RECORD_TYPE[self.type][1]])
