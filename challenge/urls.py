# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path

from rest_framework.routers import DefaultRouter

from challenge.billing.views import CallDetailViewSet, CallViewSet, CallView, BillingView, index

router = DefaultRouter()
router.register(r'calldetail', CallDetailViewSet)
router.register(r'call', CallViewSet)


urlpatterns = [
    path("", index, name="index"),
    path('admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^receivecall/', CallView.as_view()),
    url(r'^billing/', BillingView.as_view())
]
